package in.himtech.core.general;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainTest {
	public static void main(String[] args) {
		ApplicationContext applnContext = new ClassPathXmlApplicationContext(
				"applnContext.xml");

		/// Demonstration of passing value by constructor
		ConstructorArgu constArgu = applnContext.getBean("conArguId", ConstructorArgu.class);
		System.out.println(constArgu);
		System.out.println("Name: " + constArgu.getName());
		System.out.println("Salary: " + constArgu.getSalary());
		System.out.println("Age: " + constArgu.getAge());
	}
}
