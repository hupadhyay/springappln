package in.himtech.core.general;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

public class Person {
	private String Name;
	private int rate;
	private Address address;
	private Work work;
	
	public Person() {
		// TODO Auto-generated constructor stub
	}
	
	public Person(Address add, Work wrk) {
		address = add;
		work = wrk;
	}

	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public Address getAddress() {
		return address;
	}

	@Autowired
	@Qualifier(value="jhandoo")
	@Required
	public void setAddress(Address address) {
		this.address = address;
	}

	public Work getWork() {
		return work;
	}

	@Autowired
	@Required
	public void setWork(Work work) {
		this.work = work;
	}

	@Override
	public String toString() {
		return "Person [Name=" + Name + ", rate=" + rate + ", address="
				+ address + ", work=" + work + "]";
	}
}

class Address {
	private int houseNumber;
	private String locality;
	private String district;

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	@Override
	public String toString() {
		return "Address [houseNumber=" + houseNumber + ", locality=" + locality
				+ ", district=" + district + "]";
	}

}

class Work {
	private String type;
	private int efficiency;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(int efficiency) {
		this.efficiency = efficiency;
	}

	@Override
	public String toString() {
		return "Work [type=" + type + ", efficiency=" + efficiency + "]";
	}

}
