package in.himtech.core.general;

import org.springframework.context.ApplicationListener;

public class MyEventListener implements ApplicationListener<MyEvent>{

	@Override
	public void onApplicationEvent(MyEvent myevent) {
		System.out.println("Listening MyEvent: " + myevent.getEventTime());
		
	}

}
