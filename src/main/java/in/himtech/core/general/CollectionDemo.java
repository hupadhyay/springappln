package in.himtech.core.general;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CollectionDemo {
	private List<String> groceryList;
	private Set<String> countrySet;
	private Map<String, String> pinMap;
	private Properties properties;
	private String [] spicesArr; 
	
	public List<String> getGroceryList() {
		return groceryList;
	}
	
	public void setGroceryList(List<String> groceryList) {
		this.groceryList = groceryList;
	}
	
	public Set<String> getCountrySet() {
		return countrySet;
	}
	
	public void setCountrySet(Set<String> countrySet) {
		this.countrySet = countrySet;
	}
	
	public Map<String, String> getPinMap() {
		return pinMap;
	}
	
	public void setPinMap(Map<String, String> pinMap) {
		this.pinMap = pinMap;
	}
	
	public Properties getProperties() {
		return properties;
	}
	
	public void setProperties(Properties propties) {
		this.properties = propties;
	}
	
	public String[] getSpicesArr() {
		return spicesArr;
	}
	
	public void setSpicesArr(String[] spicesArr) {
		this.spicesArr = spicesArr;
	}
}
