package in.himtech.core.general;

import java.util.Date;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class MyEventSource implements ApplicationEventPublisherAware{

	private ApplicationEventPublisher applicationEventPublisher;
	
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
		
	}
	
	public void publishMyEvent(){
		Date date  = new Date();
		System.out.println("publishing time is: " + date);
		applicationEventPublisher.publishEvent(new MyEvent(this, date));
	}
}

@SuppressWarnings("serial")
class MyEvent extends ApplicationEvent {
	
	private Date eventTime;

	public MyEvent(Object source, Date time) {
		super(source);
		eventTime = time;
	}
	
	public Date getEventTime() {
		return eventTime;
	}
	
}