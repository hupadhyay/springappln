package in.himtech.core.general;

public class ConstructorArgu {
	private String Name;
	private double salary;
	private int age;
	
	public ConstructorArgu(String name, double salary, int age) {
		Name = name;
		this.salary = salary;
		this.age = age;
	}

	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "ConstructorArgu [Name=" + Name + ", salary=" + salary + ", age="
				+ age + "]";
	}
	

}
