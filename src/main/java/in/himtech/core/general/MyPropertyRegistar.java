package in.himtech.core.general;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;

public class MyPropertyRegistar implements PropertyEditorRegistrar{

	@Override
	public void registerCustomEditors(PropertyEditorRegistry per) {
		per.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("dd-MMM-yyyy"), false));
		
	}

}
