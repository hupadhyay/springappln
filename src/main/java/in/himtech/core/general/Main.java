package in.himtech.core.general;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext applnContext = new ClassPathXmlApplicationContext("applnContext.xml");
		
		// Demonstration of setting property of bean.
		PropertyArgu propertyArgu = applnContext.getBean("propArgu", PropertyArgu.class);
		System.out.println(propertyArgu);
		System.out.println("Name: " + propertyArgu.getName());
		System.out.println("Salary: " + propertyArgu.getSalary());
		System.out.println("Age: " + propertyArgu.getAge());
		
		// Demonstration of passing value by constructor
		ConstructorArgu constArgu = applnContext.getBean("conArguId", ConstructorArgu.class);
		System.out.println(constArgu);
		System.out.println("Name: " + constArgu.getName());
		System.out.println("Salary: " + constArgu.getSalary());
		System.out.println("Age: " + constArgu.getAge());
		
		// Demonstration of setting property of bean.
		PropertyArgu propertyArguShortcut = applnContext.getBean("propArguShortcut", PropertyArgu.class);
		System.out.println(propertyArguShortcut);
		
		// Demonstration of Collections
		CollectionDemo collDemo = applnContext.getBean("collectionId", CollectionDemo.class);
		List<String> listofItems = collDemo.getGroceryList();
		System.out.print("Grocery Items: ");
		for(String item : listofItems){
			System.out.printf("%s, ", item);
		}
		System.out.println();
		Set<String> listCountries = collDemo.getCountrySet();
		System.out.print("Name of Countries:");
		for(String country : listCountries){
			System.out.printf("%s, ", country);
		}
		System.out.println();
		Map<String, String> mapPin = collDemo.getPinMap();
		Set<String> keys = mapPin.keySet();
		System.out.print("Map of location and postal pincode: ");
		for(String key : keys){
			System.out.printf("(%s:%s), ", key, mapPin.get(key));
		}
		System.out.println();
		Properties prop = collDemo.getProperties();
		Set<Object> keySet = prop.keySet();
		System.out.print("Properties Items: ");
		for(Object key : keySet){
			System.out.printf("(%s:%s), ", key, prop.get(key));
		}
		System.out.println();
		String[] spices = collDemo.getSpicesArr();
		System.out.print("Spices Array: ");
		for(String str : spices){
			System.out.printf("%s, ", str);
		}
		System.out.println();
		
		Person p = applnContext.getBean("person", Person.class);
		System.out.println(p);
		
		//Application event generation.
		MyEventSource source = applnContext.getBean("eventSource", MyEventSource.class);
		source.publishMyEvent();
		source.publishMyEvent();
		
		// Traditional way of converting.
		ProductRanking prodRank = applnContext.getBean("prodRank", ProductRanking.class);
		System.out.println("Product Name: " + prodRank.getProductName());
		System.out.println("From Date: " + prodRank.getFromDate());
		System.out.println("To Date: " + prodRank.getToDate());
		
		// Value Conversion using of Property Editors
		ProductRanking prodRank1 = applnContext.getBean("prodRanking", ProductRanking.class);
		System.out.println("Product Name: " + prodRank1.getProductName());
		System.out.println("From Date: " + prodRank1.getFromDate());
		System.out.println("To Date: " + prodRank1.getToDate());
		
		// Closing of application context container.
		((AbstractApplicationContext)applnContext).close();
				
	}
}
