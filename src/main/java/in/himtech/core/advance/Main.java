package in.himtech.core.advance;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		ApplicationContext applnContext = new ClassPathXmlApplicationContext("applnContextAdv.xml");
		
		Product productTv = applnContext.getBean("productCreator", Product.class);
		System.out.println(productTv);
		
		Product prodTV = applnContext.getBean("bulbIns", Product.class);
		System.out.println(prodTV);
		
		// Accessing of static field.
		String nation = applnContext.getBean("desh", String.class);
		System.out.println("Static filed value: " + nation);
		
		// Accessing of instance filed.
		String category = applnContext.getBean("catId", String.class);
		System.out.println("Instance filed value: " + category);
		
		//Initialization and destroy
		EntryMaker entryMaker = applnContext.getBean("entryMaker", EntryMaker.class);
		entryMaker.makeEntry();
		
		// BeanNameAware
		Product prod = applnContext.getBean("product", Product.class);
		System.out.println("Bean Name: " + prod.getBeanId());
		
		//Using resource to read external file
		MyBanner myBanner = applnContext.getBean("mybanner", MyBanner.class);
		myBanner.showBanner();
		
		Shape shape = applnContext.getBean("shape", Shape.class);
		System.out.println("Name of shape: " + shape.getName());
		System.out.println("Sides of shape: " + shape.getSides());
		System.out.println("Message: " + shape.getMessage());
		
		//Internationlization: reading properties files using applicaiton context:
		System.out.println(applnContext.getMessage("general.greeting", null, Locale.getDefault()));
		System.out.println(applnContext.getMessage("english.greeting", new Object[]{"Morning"}, Locale.getDefault()));
		System.out.println(applnContext.getMessage("leader.greeting", new Object[]{"bahano", "bhaiyon", "namaste"}, Locale.getDefault()));
		
		//Internationalization: reading thorugh message source object.
		ImplMessageSource msgSource = applnContext.getBean("msgbundle", ImplMessageSource.class);
		System.out.println(msgSource.getMessage("general.greeting", null));
		System.out.println(msgSource.getMessage("english.greeting", new Object[]{"Morning"}));
		System.out.println(msgSource.getMessage("leader.greeting", new Object[]{"bahano", "bhaiyon", "namaste"}));
		
		
		// Closing of application context container.
		((ConfigurableApplicationContext)applnContext).close();
	}
}
