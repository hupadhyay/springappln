package in.himtech.core.advance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

public class BannerLoader implements ResourceLoaderAware{

	private ResourceLoader resourceLoader;

	public void displayBanner(){
		Resource resource = resourceLoader.getResource("classpath:mybanner.txt");
		try {
			BufferedReader bReader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			String str = null;
			while((str = bReader.readLine()) != null){
				System.out.println(str);
			}
			bReader.close();
		} catch (IOException e) {
			System.out.println("Bananer is not loaded.");
			e.printStackTrace();
		} 
	}
	
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		
		this.resourceLoader = resourceLoader;
	}

}
