package in.himtech.core.advance;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class EntryMaker {
	private String fileName;
	private String filePath;
	private BufferedWriter bw = null;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public void createWriter(){
		File file = new File(filePath, fileName);
		try {
			bw = new BufferedWriter(new FileWriter(file, true));
			System.out.println("Writer created");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void releaseWriter(){
		try {
			bw.close();
			System.out.println("Writer closed");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void makeEntry(){
		try {
			bw.write(String.valueOf(new Date()));
			bw.write("\n");
			System.out.println("Entry Done");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
