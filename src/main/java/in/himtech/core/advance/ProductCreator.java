package in.himtech.core.advance;

public class ProductCreator {
	public static Product createProduct(String type)
			throws IllegalArgumentException {
		Product product = null;

		switch (type) {
		case "buld":
			product = new Product(type, 32.50, "electric");
		case "tv":
			product = new Product(type, 64000, "electronics");
		}
		if (product == null) {
			throw new IllegalArgumentException();
		}
		return product;
	}
}
