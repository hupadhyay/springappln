package in.himtech.core.advance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.core.io.Resource;

public class MyBanner {
	private Resource banner;

	public Resource getBanner() {
		return banner;
	}

	public void setBanner(Resource banner) {
		this.banner = banner;
	}
	
	public void showBanner(){
		try {
			BufferedReader bReader = new BufferedReader(new InputStreamReader(banner.getInputStream()));
			String str = null;
			while((str = bReader.readLine()) != null){
				System.out.println(str);
			}
			bReader.close();
		} catch (IOException e) {
			System.out.println("Bananer is not loaded.");
			e.printStackTrace();
		} 
	}
}
