package in.himtech.core.advance;

import java.util.Map;

public class InsProductCreator {
	private Map<String, Product> mapProd;
	
	public void setMapProd(Map<String, Product> mapProd) {
		this.mapProd = mapProd;
	}
	
	public Map<String, Product> getMapProd() {
		return mapProd;
	}

	public Product createProduct(String key) throws IllegalArgumentException{
		Product prod;
		if(mapProd.containsKey(key)){
			prod = mapProd.get(key);
		} else{
			throw new IllegalArgumentException("invalid key");
		}
		return prod;
	}
}
