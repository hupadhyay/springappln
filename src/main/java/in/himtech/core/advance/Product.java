package in.himtech.core.advance;

import org.springframework.beans.factory.BeanNameAware;

import sun.security.action.GetBooleanAction;

public class Product implements BeanNameAware{
	public static String nation = "Bharat";
	private String type;
	private double price;
	private String category;
	private String beanId;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}

	public Product(String type, double price, String category) {
		super();
		this.type = type;
		this.price = price;
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Product [type=" + type + ", price=" + price + ", category="
				+ category + "]";
	}
	
	public String getBeanId() {
		return beanId;
	}

	@Override
	public void setBeanName(String arg0) {
		beanId = arg0;
		
	}
}
