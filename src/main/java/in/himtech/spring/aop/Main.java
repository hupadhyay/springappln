package in.himtech.spring.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applnContextAop.xml");
		
		ArithmeticCalculator arithmeticCalculator = context.getBean("airthCalcs", ArithmeticCalculator.class);
		double output = arithmeticCalculator.add(5, 7);
		System.out.println("Addition: " + output);
	}

}
