package in.himtech.spring.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class CalculatorAspect {

	@Before("execution(* ArithmeticCalculator.add(..))")
	public void logBefore() {
		System.out.println("Simple Before Advice");
	}

	@Before("execution(* *.*(..))")
	public void logBefore(JoinPoint joinpoint) {
		System.out.println("Before Advice: Method Name: "
				+ joinpoint.getSignature().getName() + ", Argument: "
				+ joinpoint.getArgs());
	}

	@After("execution(* *.*(..))")
	public void logAfter(JoinPoint joinPoint) {
		System.out.println("After Advice: Method Name: "
				+ joinPoint.getSignature().getName());
	}

	@AfterReturning(pointcut = "execution(* *.*(..))", returning = "result")
	public void logAfterReturning(JoinPoint joinPoint, Object result) {
		System.out.println("After Advice Returning: Method Name: "
				+ joinPoint.getSignature().getName() + " ends with " + result);
	}

	/** For all type of exception. */
	@AfterThrowing(pointcut = "execution(* *.*(..))", throwing = "e")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
		System.out.println("An exception " + e + " has been thrown in "
				+ joinPoint.getSignature().getName() + "()");
	}

	/** When interested in perticular type of exception. */
	@AfterThrowing(pointcut = "execution(* *.*(..))", throwing = "e")
	public void logAfterThrowingExcep(JoinPoint joinPoint,
			IllegalArgumentException e) {
		System.out.println("An exception " + e + " has been thrown in "
				+ joinPoint.getSignature().getName() + "()");
	}

	@Around("execution(* *.*(..))")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		System.out.println("The method " + joinPoint.getSignature().getName()
				+ "() begins with " + Arrays.toString(joinPoint.getArgs()));
		try {
			Object result = joinPoint.proceed();
			System.out.println("The method " + joinPoint.getSignature().getName()
					+ "() ends with " + result);
			return result;
		} catch (IllegalArgumentException e) {
			System.err.println("Illegal argument "
					+ Arrays.toString(joinPoint.getArgs()) + " in "
					+ joinPoint.getSignature().getName() + "()");
			throw e;
		}
	}
}
